<?php
	header('Content-type: text/json');

	require_once('inc/class.Car.php');
	require_once('inc/class.Maker.php');
	require_once('inc/db.php');

	$car = new Car($db_pdo);
	$cars = $car->select_all('array');

	$car_exists = false;
	if(sizeof($cars) > 0 )
		$car_exists = true;

	if($car_exists) 
		echo json_encode(array(
			'success' => true, 
			'cars' => $cars));
	else
		echo json_encode(array(
			'success' => false, 
			'message' => 'Det finns inga bilar i systemet!'));