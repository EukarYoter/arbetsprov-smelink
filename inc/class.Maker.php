<?php

class Maker {
	private $db;
	private $table;

	private $maker_id;
	private $name;

	function __construct($db_pdo = null) {
		$this->db = $db_pdo;
		$this->table = "makers";
	}

	public function get_maker_id() { return $this->maker_id; }
	public function get_name() { return $this->name; }

	public function set_maker_id($maker_id) { $this->maker_id = $maker_id; }
	public function set_name($name) { $this->name = $name; }

	public function select_name_by_id($id) {
		$statement = $this->db->prepare('SELECT name FROM '.$this->table.' WHERE maker_id = :id');
		$statement->execute(array(':id' => $id));
		$row = $statement->fetch(); 

	    return $row['name'];
	}
}