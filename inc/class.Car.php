<?php

class Car {
	private $db;
	private $table;

	private $car_id;
	private $maker_id;
	private $maker;
	private $color;
	private $year;

	function __construct($db_pdo = null) {
		$this->db = $db_pdo;
		$this->table = "cars";
	}

	public function get_car_id() {return $this->car_id;	}
	public function get_maker_id() { return $this->maker_id; }
	public function get_color() { return $this->color; }
	public function get_year() { return $this->year; }
	public function get_maker() { return $this->maker; }

	public function set_car_id($car_id) { $this->car_id = $car_id; }
	public function set_maker_id($maker_id) { $this->maker_id = $maker_id; }
	public function set_color($color) { $this->color = $color; }
	public function set_year($year) { $this->year = $year; }
	public function set_maker($maker) { $this->maker = $maker; }

	public function select_all($data = 'object') {
		$cars = array();
		$sql = 'SELECT * FROM '.$this->table;
	    foreach ($this->db->query($sql) as $row) {
	    	// if the should contain objects
	    	if($data == 'object' ) {
		    	$car = new Car();
		        $car->set_car_id($row['car_id']);
		        $car->set_maker_id($row['maker_id']);
		        $car->set_color($row['color']);
		        $car->set_year($row['year']);

		        $maker = new Maker($this->db);
		        $maker_name = $maker->select_name_by_id($row['maker_id']);
		        $car->set_maker($maker_name);
	        // if the should contain array
	        } else if($data == 'array') {
		        $car['id'] = $row['car_id'];
		        $car['maker_id'] = $row['maker_id'];
		        $car['color'] = $row['color'];
		        $car['year'] = $row['year'];

		        $maker = new Maker($this->db);
		        $maker_name = $maker->select_name_by_id($row['maker_id']);
		        $car['maker'] = $maker_name;
	        }

			$cars[] = $car;
	    }

	    return $cars;
	}
}