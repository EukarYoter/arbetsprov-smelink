<!doctype HTML>
<html lang="sv">
<head>
	<meta charset="UTF-8">
	<title>Arbetsprov SmeLink</title>

	<link rel="stylesheet" href="/assets/css/style.css">
	<link rel="author" href="/humans.txt" />
</head>
<body>
	
	<div class="content">
		<div id="ajax-loader"></div>
		<div id="ajax-message"></div>
		<div id="cars"></div>

		<div class="btn" id="load-cars">Ladda bilar</div>
	</div>

	<div class="grass-sky"></div>
	<div class="road">
		<div class="car-movement fast-car">
			<div class="car">
			    <div class="front part-red">
			    	<div class="light"></div>
			        <div class="wheel"></div>
			        <div class="antennae"></div>
			    </div>
			    <div class="middle part-red">
			        <div class="windows">
			            <div class="glass"></div>
			            <div class="glass"></div>
			        </div>
			    </div>
			    <div class="back part-red">
			        <div class="wheel"></div>
			    </div>
			</div>
		</div>
		<div class="car-movement slow-car">
			<div class="car">
			    <div class="front part-blue">
			    	<div class="light"></div>
			        <div class="wheel"></div>
			        <div class="antennae"></div>
			    </div>
			    <div class="middle part-blue">
			        <div class="windows">
			            <div class="glass"></div>
			            <div class="glass"></div>
			        </div>
			    </div>
			    <div class="back part-blue">
			        <div class="wheel"></div>
			    </div>
			</div>
		</div>
	</div>
	<div class="footer">
		<p>Skapad av: Christoffer Rydberg</p>
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="/assets/js/app.js"></script>
</body>
</html>