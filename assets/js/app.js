var do_car_table = function(cars) {
	var table = $('#cars table');
	if(table.length) {
		table.remove();
		$('#cars h1').remove();
	}

	$('#ajax-message').empty();
	var table = document.createElement('table'),
		thead = document.createElement('thead'),
		tbody = document.createElement('tbody'),
		tr_head = document.createElement('tr');

	$(table).addClass('car-table');

	$(tr_head).append('<th>Bil ID</th>');
	$(tr_head).append('<th>Tillverkare</th>');
	$(tr_head).append('<th>Färg</th>');
	$(tr_head).append('<th>Årsmodell</th>');
	$(thead).append(tr_head);
	$(table).append(thead);

	$.each(cars, function(i, value) {
		var tr = document.createElement('tr');
		$(tr).append('<td>'+value.id+'</td>');
		$(tr).append('<td>'+value.maker+'</td>');
		$(tr).append('<td>'+value.color+'</td>');
		$(tr).append('<td>'+value.year+'</td>');

		$(tbody).append(tr);
	});
	$(table).append(tbody);
	$('#cars').append('<h1>Bilar</h1>');
	$('#cars').append(table);
	$('#cars').show();
}

$(function() {
	$('#load-cars').on('click', function() {
		var that = this;

		$.ajax({
			url: 'get_cars.php',
			type: 'get',
			beforeSend: function() {
				$('#ajax-loader').show();
			},
			success: function(data) {
				$('#ajax-loader').hide();
				if(data.success) {

					do_car_table(data.cars);

					$(that).addClass('btn-reload');
					$(that).html('Ladda om bilar');
				} else {
					$('#cars').empty();
					$('#ajax-message').empty();
					$('#ajax-message').prepend(data.message);					
				}
			}
		});
	});
});